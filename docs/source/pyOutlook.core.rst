pyOutlook.core package
======================

Submodules
----------

pyOutlook.core.folders module
-----------------------------

.. automodule:: pyOutlook.core.folders
    :members:
    :undoc-members:
    :show-inheritance:

pyOutlook.core.main module
--------------------------

.. automodule:: pyOutlook.core.main
    :members:
    :undoc-members:
    :show-inheritance:

pyOutlook.core.message module
-----------------------------

.. automodule:: pyOutlook.core.message
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pyOutlook.core
    :members:
    :undoc-members:
    :show-inheritance:
