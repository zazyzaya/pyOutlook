.. _pyOutlook:

Outlook Account
---------------

.. autoclass:: pyOutlook.core.main.OutlookAccount
    :members:

.. _MessageAnchor:

Message
-------

.. autoclass:: pyOutlook.core.message.Message
    :members:
    :undoc-members:

.. _NewMessageAnchor:

NewMessage
----------

.. autoclass:: pyOutlook.internal.createMessage.NewMessage
    :members:


Folder
------

.. autoclass:: pyOutlook.core.folders.Folder
    :members:

